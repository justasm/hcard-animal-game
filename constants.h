#include "Arduino.h"

const int NUM_SINKS = 4;
const int NUM_LEDS_PER_SINK = 2;
const int NUM_SOUNDS = 5; // NUM_SINKS + 1 victory tune

const int RANDOM_SRC = A6;
const int VICTORY_SOUND = 4;

const int SHEEP = 0;
const int PIG = 1;
const int HORSE = 2;
const int COW = 3;

/*
OUTPUT
----------------------------------------------------
*/
// sink LEDs (xNUM_LEDS_PER_SINK ea, xNUM_SINKS)
int const sinkLeds[NUM_SINKS][NUM_LEDS_PER_SINK] =
    {{35, 42}, {36, 36}, {34, 50}, {37, 40}};
// buzzer (xNUM_SINKS)
int const buzzers[NUM_SINKS] = {10, 12, 11, 9};
// sound module (xNUM_SOUNDS)
int const sounds[NUM_SOUNDS] = {27, 28, 26, 24, 25};

// Bluetooth settings
const int BT_BAUD_RATE = 9600; // programmed on BT chip using AT mode
// we will use Serial3 of our Mega2560 - as a result, the TX & RX pins below MUST NOT CHANGE!!!
// if pins need to be changed, other options are to use Serial2 or the SoftwareSerial library.
const int TX3 = 14; // connected to RXD of BT chip
const int RX3 = 15; // connected to TXD of BT chip
const byte DATA_HEADER = 0xDA;

/*
INPUT
----------------------------------------------------
*/
// potentiometers from hinges (xNUM_SINKS)
int const pots[NUM_SINKS] = {A3, A1, A2, A0};
// switches for ball detection (xNUM_SINKS)
// these are used for interrupts; in this case, they are Mega2560-specific!
// see http://arduino.cc/en/Reference/AttachInterrupt
const int switch0 = 21;
const int switch1 = 20;
const int switch2 = 19;
const int switch3 = 18;
const int switch0Interrupt = 2;
const int switch1Interrupt = 3;
const int switch2Interrupt = 4;
const int switch3Interrupt = 5;

/*
STATE
----------------------------------------------------
*/
const unsigned long SUCCESS_DURATION_MILLIS = 9000;
const unsigned long FAILURE_DURATION_MILLIS = 2000;
const unsigned long FAILURE_BEEPER_MILLIS = 1000;

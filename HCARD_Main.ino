/*
Animal Game Program
for HCARD 2014
Christian, Javier, Justas, Niccolo
*/
#include "constants.h"
#include "pitches.h"
#include "state.h"

/*
INPUT
----------------------------------------------------
*/
// array to store the latest potentiometer values
int potValues[NUM_SINKS];

/*
STATE
----------------------------------------------------
see "state.h" for more
*/
State state;

unsigned long stateStartTimeMillis;
int targetSink = -1;

void switchToState(State newState){
  state = newState;
  btTransmitNewState(newState);
  stateStartTimeMillis = millis();
}

unsigned long stateElapsedMillis(){
  return millis() - stateStartTimeMillis;
}

/*
DEBUG
----------------------------------------------------
*/
String STATE_SEPARATOR = "------------------------";

/*
SETUP
----------------------------------------------------
*/
void setup() {
  // for debugging
  Serial.begin(9600);
  
  // Bluetooth
  Serial3.begin(BT_BAUD_RATE);
  
  // setup pin modes
  for(int i = 0; i < NUM_SINKS; i++){
    pinMode(buzzers[i], OUTPUT);
    for(int j = 0; j < NUM_LEDS_PER_SINK; j++){
      pinMode(sinkLeds[i][j], OUTPUT);
    }
    
    pinMode(pots[i], INPUT);
  }
  for(int i = 0; i < NUM_SOUNDS; i++){
    pinMode(sounds[i], OUTPUT);
    digitalWrite(sounds[i], HIGH); // HIGH by default
  }
  setupSwitches();
  
  // if analog input pin 0 is unconnected, this will return random
  // analog noise as the seed
  // this ensures different random sequence on each start-up
  randomSeed(analogRead(RANDOM_SRC));
  
  // start in the default state
  switchToState(CHOOSING);
  
  // DEBUG
  Serial.println("SETUP COMPLETE, BEGINNING GAME");
  Serial.println(STATE_SEPARATOR);
}

void setupSwitches(){
  attachInterrupt(switch0Interrupt, onSwitch0, FALLING);
  attachInterrupt(switch1Interrupt, onSwitch1, FALLING);
  attachInterrupt(switch2Interrupt, onSwitch2, FALLING);
  attachInterrupt(switch3Interrupt, onSwitch3, FALLING);
}

void playSound(int sound){
  digitalWrite(sound, LOW);
  delayMicroseconds(16383);
  digitalWrite(sound, HIGH);
}

/*
MAIN PROGRAM LOOP
----------------------------------------------------
*/
void loop() {
  switch(state){
    case CHOOSING:
    default:{
      // pick a new, random target - necessarily different from the last!
      int lastSink = targetSink;
      while(targetSink == lastSink){
        targetSink = random(NUM_SINKS);
      }
      // now that we have a target, start the game!
      switchToState(WAITING);
      
      // tell any listeners
      btTransmitNewTarget(targetSink);
      
      // play the target animal's sound
      playSound(sounds[targetSink]);
      
      // DEBUG
      Serial.print("NEW TARGET: ");
      Serial.println(targetSink);
      Serial.println(STATE_SEPARATOR);
      break;
    }
    case WAITING:{
      // the program will spend the majority of the time here
      
      for(int i = 0; i < NUM_SINKS; i++){
        potValues[i] = analogRead(pots[i]);
        
        btTransmitPotValue(i, potValues[i]);
      }
      
      boolean lightToggle = stateElapsedMillis() % 1000 > 500;
      boolean soundToggle = stateElapsedMillis() % 2000 < 50;
      
      // toggle lights for active animal
      for(int i = 0; i < NUM_SINKS; i++){
        for(int j = 0; j < NUM_LEDS_PER_SINK; j++){
          if(i == targetSink){
            if(j % 2 == 0){
              digitalWrite(sinkLeds[i][j], lightToggle ? HIGH : LOW);
            } else {
              digitalWrite(sinkLeds[i][j], lightToggle ? LOW : HIGH);
            }
          } else {
            digitalWrite(sinkLeds[i][j], LOW);
          }
        }
        
        if(i == targetSink){
          if(soundToggle){
            tone(buzzers[i], NOTE_C4);
          } else {
            noTone(buzzers[i]);
          }
        }
      }
      
      break;
    }
    case SUCCESS:{
      clearState(targetSink);
      boolean lightToggle = stateElapsedMillis() % 200 > 100;
      for(int i = 0; i < NUM_LEDS_PER_SINK; i++){
        digitalWrite(sinkLeds[targetSink][i], lightToggle ? HIGH : LOW);
      }
      
      if(stateElapsedMillis() > SUCCESS_DURATION_MILLIS){
        // a little less celebration, a little more action!
        switchToState(CHOOSING);
        // DEBUG
        Serial.println("SUCCESS FEEDBACK COMPLETE, CHOOSING NEW TARGET");
        Serial.println(STATE_SEPARATOR);
      }
      break;
    }
    case FAILURE:{
      clearState(targetSink);
      // highlight correct one
      boolean lightToggle = stateElapsedMillis() % 100 > 50;
      for(int i = 0; i < NUM_LEDS_PER_SINK; i++){
        digitalWrite(sinkLeds[targetSink][i], lightToggle ? HIGH : LOW);
      }
      
      // play negative scale
      if(stateElapsedMillis() % FAILURE_DURATION_MILLIS < FAILURE_BEEPER_MILLIS / 3){
        tone(buzzers[targetSink], NOTE_C2);
      } else if(stateElapsedMillis() % FAILURE_DURATION_MILLIS < 2 * FAILURE_BEEPER_MILLIS / 3){
        tone(buzzers[targetSink], NOTE_F2);
      } else if(stateElapsedMillis() % FAILURE_DURATION_MILLIS < FAILURE_BEEPER_MILLIS){
        tone(buzzers[targetSink], NOTE_G2);
      } else {
        noTone(buzzers[targetSink]);
      }
      
      if(stateElapsedMillis() > FAILURE_DURATION_MILLIS){
        // enough wallowing in self pity, keep playing!
        switchToState(WAITING);
        // DEBUG
        Serial.println("FAILURE FEEDBACK COMPLETE, WAITING FOR SAME TARGET");
        Serial.println(STATE_SEPARATOR);
      }
      break;
    }
  }
}

// Clears any active buzzers / LEDs for the given sink
void clearState(int sink){
  if(sink < 0) return;
  noTone(buzzers[sink]);
  for(int i = 0; i < NUM_LEDS_PER_SINK; i++){
    digitalWrite(sinkLeds[sink][i], LOW);
  }
}

/*
EVENTS
----------------------------------------------------
*/
void onSwitch(int which){
  // since no debouncing is performed, this state check ensures the state switch occurs only once / toggle
  if(state == WAITING){
    if(which == targetSink){
      // play victory sound on speaker
      playSound(sounds[VICTORY_SOUND]);
      
      switchToState(SUCCESS);
      
      // DEBUG
      Serial.print("SUCCESS! PLAYER SELECTED LID ");
      Serial.print(which);
      Serial.println(", INITIATING FEEDBACK.");
    } else {
      switchToState(FAILURE);
      
      // DEBUG
      Serial.print("FAILURE! PLAYER SELECTED LID ");
      Serial.print(which);
      Serial.println(", TRY AGAIN.");
    }
  }
  
  // DEBUG
  Serial.print("Switch "); Serial.print(which); Serial.println(" was just pressed!");
}

// Delegate all switch interrupts to one method, we'll handle each one the same
void onSwitch0(){ onSwitch(0); }
void onSwitch1(){ onSwitch(1); }
void onSwitch2(){ onSwitch(2); }
void onSwitch3(){ onSwitch(3); }

/*
BLUETOOTH
----------------------------------------------------
Data format:
DATA_HEADER [1 byte, constant]
DATA_LENGTH [1 byte]
DATA        [DATA_LENGTH bytes]
*/
byte MSG_STATE = 1;
byte MSG_TARGET = 2;
byte MSG_POT = 3;

byte twoBytes[2]; // for storing result of int16 to two byte conversion
void btTransmitPotValue(byte potIndex, int value){
  int16ToBytes(value, twoBytes); // one int16 == 2 bytes
  byte dataLength = 1 + 1 + 2; // msg type [1 byte] + pot index [1 byte] + pot value [2 bytes]
  
  Serial3.write(DATA_HEADER);
  Serial3.write(dataLength);
  Serial3.write(MSG_POT);
  Serial3.write(potIndex);
  Serial3.write(twoBytes[0]);
  Serial3.write(twoBytes[1]);
}

void btTransmitNewState(State state){
  int16ToBytes(state, twoBytes); // one int16 == 2 bytes
  byte dataLength = 1 + 1; // msg type [1 byte] + state [lower 1 byte]
  
  Serial3.write(DATA_HEADER);
  Serial3.write(dataLength);
  Serial3.write(MSG_STATE);
  Serial3.write(twoBytes[1]);
}

void btTransmitNewTarget(byte target){
  byte dataLength = 1 + 1; // msg type [1 byte] + target [1 byte]
  
  Serial3.write(DATA_HEADER);
  Serial3.write(dataLength);
  Serial3.write(MSG_TARGET);
  Serial3.write(target);
}

// converts a 16-bit / 2-byte int to 2 separate bytes, big-endian
void int16ToBytes(int x, byte out[]){
  out[0] = (byte) (x >> 8);
  out[1] = (byte) x;
}

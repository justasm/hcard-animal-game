typedef enum State{
  CHOOSING,
  WAITING,
  SUCCESS,
  FAILURE
};

void switchToState(State newState);
